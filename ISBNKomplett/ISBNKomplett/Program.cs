﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ISBNKomplett
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] isbns;
            CreateISBNs(out isbns);
            CheckAllIsbns(isbns);
            Console.Read();
        }

        private static void CreateISBNs(out string[] isbns)
        {
            isbns = new string[]{
                "3-499-13599-X",
                "3-446-19313-8",
                "0-7475-5100-6",
                "1-57231-422-2",
                "349913599X",
                "1-234-56789-0",
                "978-3-12-732320-7",
                "9783960090724",
                "978-3864902857",
                "9783446437234",
                "978-9-08-998815-0",
                "978-9-08-998716-7"
            };
        }

        private static void CheckAllIsbns(string[] isbns)
        {
            foreach (string isbn in isbns)
            {
                int checkDigit13 = 0;
                char checkDigit10 = '0';
                if (isbn.Replace("-","").Length > 10)
                {
                    IsbnCheckDigit(isbn, ref checkDigit13);
                    Display(IsValid(isbn, checkDigit13), isbn, checkDigit13);
                }
                else
                {
                    IsbnCheckDigit(isbn, ref checkDigit10);
                    Display(IsValid(isbn, checkDigit10), isbn, checkDigit10);
                }

            }
        }

        private static void IsbnCheckDigit(string isbn, ref int checkDigit)
        {
            isbn = isbn.Replace("-", "");
            int i = 0;
            while (i < isbn.Length - 1)
            {
                if (i % 2 == 0)
                    checkDigit += int.Parse(isbn[i].ToString());
                else
                    checkDigit += int.Parse(isbn[i].ToString()) * 3;
                i++;
            }
            checkDigit = (10 - (checkDigit % 10)) % 10;
        }

        private static void IsbnCheckDigit(string isbn, ref char checkDigit)
        {
            isbn = isbn.Replace("-", "");
            int i = 1;
            int ergIsbn = 0;

            foreach (char character in isbn)
            {
                if (character != '-')
                {
                    if (i < 10)
                    {
                        ergIsbn += i * (int)char.GetNumericValue(character);
                        i += 1;
                    }
                    else
                        ergIsbn = ergIsbn % 11;      
                }
            }
            if (ergIsbn == 10)
                checkDigit = 'X';
            else
                checkDigit = ergIsbn.ToString()[0];
        }

        private static bool IsValid(string isbn, int checkdigit)
        {
            return (int.Parse(isbn[isbn.Length - 1].ToString()).Equals(checkdigit));
        }

        private static bool IsValid(string isbn, char checkdigit)
        {
                return (isbn[isbn.Length - 1].ToString()[0].Equals(checkdigit));
        }

        private static void Display(bool isValid, string isbn, int checkDigit)
        {
            Console.WriteLine("Valid: " + isValid);
            Console.WriteLine("ISBN-13: " + isbn);
            Console.WriteLine("Check digit: " + checkDigit);
            Console.WriteLine("------------------------------");
        }

        private static void Display(bool isValid, string isbn, char checkDigit)
        {
            Console.WriteLine("Valid: " + isValid);
            Console.WriteLine("ISBN-10: " + isbn);
            Console.WriteLine("Check digit: " + checkDigit);
            Console.WriteLine("------------------------------");
        }
    }
}
